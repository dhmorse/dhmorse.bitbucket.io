var annotated_dup =
[
    [ "comboDriver", null, [
      [ "Controller", "classcomboDriver_1_1Controller.html", "classcomboDriver_1_1Controller" ]
    ] ],
    [ "encoderDriver", null, [
      [ "Encoder", "classencoderDriver_1_1Encoder.html", "classencoderDriver_1_1Encoder" ]
    ] ],
    [ "lab01", null, [
      [ "Conditions", "classlab01_1_1Conditions.html", "classlab01_1_1Conditions" ]
    ] ],
    [ "lab02", null, [
      [ "RxnGame", "classlab02_1_1RxnGame.html", "classlab02_1_1RxnGame" ]
    ] ],
    [ "lab03_main", null, [
      [ "ADCTestObj", "classlab03__main_1_1ADCTestObj.html", "classlab03__main_1_1ADCTestObj" ]
    ] ],
    [ "lab04_mcp9808", null, [
      [ "mcp9808", "classlab04__mcp9808_1_1mcp9808.html", "classlab04__mcp9808_1_1mcp9808" ]
    ] ],
    [ "lab07", null, [
      [ "TouchPanel", "classlab07_1_1TouchPanel.html", "classlab07_1_1TouchPanel" ]
    ] ],
    [ "lab08_encoderDriver", null, [
      [ "Encoder", "classlab08__encoderDriver_1_1Encoder.html", "classlab08__encoderDriver_1_1Encoder" ]
    ] ],
    [ "lab08_motorDriver", null, [
      [ "MotorDriver", "classlab08__motorDriver_1_1MotorDriver.html", "classlab08__motorDriver_1_1MotorDriver" ]
    ] ],
    [ "motorDriver", null, [
      [ "MotorBase", "classmotorDriver_1_1MotorBase.html", "classmotorDriver_1_1MotorBase" ],
      [ "DCMotor", "classmotorDriver_1_1DCMotor.html", "classmotorDriver_1_1DCMotor" ]
    ] ],
    [ "touchDriver", null, [
      [ "TouchPanel", "classtouchDriver_1_1TouchPanel.html", "classtouchDriver_1_1TouchPanel" ]
    ] ],
    [ "touchy", null, [
      [ "touchy", "classtouchy_1_1touchy.html", "classtouchy_1_1touchy" ]
    ] ]
];