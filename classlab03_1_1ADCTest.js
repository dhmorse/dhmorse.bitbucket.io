var classlab03_1_1ADCTest =
[
    [ "__init__", "classlab03_1_1ADCTest.html#ac453f749969582ffe571ee3337fda9e8", null ],
    [ "readADC", "classlab03_1_1ADCTest.html#aa8ec0f74f6e474965d544ebd65855426", null ],
    [ "writeOutADC", "classlab03_1_1ADCTest.html#aa0c8b81fc487dc95335493e83ca87ba9", null ],
    [ "adc", "classlab03_1_1ADCTest.html#a5ab90b2627b266f5a37bf644261b48a9", null ],
    [ "BTN_pressed", "classlab03_1_1ADCTest.html#a2dfd1e838d020e5d53ce2f9258bc3ff3", null ],
    [ "buff", "classlab03_1_1ADCTest.html#af449bb6646f5d96a1d8e3fc8d442f494", null ],
    [ "comm", "classlab03_1_1ADCTest.html#a35cc5e163d24b18cac0844f2335bfbf2", null ],
    [ "t_end", "classlab03_1_1ADCTest.html#a0c61ec008657d38077814432506d5502", null ],
    [ "t_start", "classlab03_1_1ADCTest.html#a7be9e273d5328dedb0ef6708b35cb06b", null ],
    [ "thresh_max", "classlab03_1_1ADCTest.html#a2e1d49fb7b5a7348e856208e85e8ce5c", null ],
    [ "thresh_min", "classlab03_1_1ADCTest.html#a825d5dcb445274b605a6f42ff2feac24", null ],
    [ "timer", "classlab03_1_1ADCTest.html#afabc632bf142e7c0675159e558c4cc13", null ],
    [ "u_LED", "classlab03_1_1ADCTest.html#acb3219e1d17981f519c2115a8e31e5b3", null ]
];