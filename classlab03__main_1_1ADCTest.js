var classlab03__main_1_1ADCTest =
[
    [ "__init__", "classlab03__main_1_1ADCTest.html#a08e80e4f0b1ba46159c7c8d59c294cee", null ],
    [ "readADC", "classlab03__main_1_1ADCTest.html#a0bcdf99dca183a1bd3a2466fd77771a9", null ],
    [ "writeOutADC", "classlab03__main_1_1ADCTest.html#a59818d257cf8fc67c257c8769906b49f", null ],
    [ "adc", "classlab03__main_1_1ADCTest.html#a50f98669518a340282113f6ae1bf044f", null ],
    [ "BTN_pressed", "classlab03__main_1_1ADCTest.html#a6c3a988de7d98381536916dae190345b", null ],
    [ "buff", "classlab03__main_1_1ADCTest.html#af36dd401a63e1e4828790af0b27aca79", null ],
    [ "comm", "classlab03__main_1_1ADCTest.html#a139ea5cef6a535fc6ae3fc3b9058b8d7", null ],
    [ "t_end", "classlab03__main_1_1ADCTest.html#a33b33b406b407d743af6151e4b8b0d07", null ],
    [ "t_start", "classlab03__main_1_1ADCTest.html#ab5ba8c88465348885f4b2dd478a57bd7", null ],
    [ "thresh_max", "classlab03__main_1_1ADCTest.html#a18a6eb562257f5df53b6708571ec2647", null ],
    [ "thresh_min", "classlab03__main_1_1ADCTest.html#a2dee95a4e09aca5ff4fba2661c3be78d", null ],
    [ "timer", "classlab03__main_1_1ADCTest.html#a231b1daef1da818b44fd06eb9cc654f4", null ],
    [ "u_LED", "classlab03__main_1_1ADCTest.html#a8183cb52bb2e5161512a972f6e743b4c", null ]
];