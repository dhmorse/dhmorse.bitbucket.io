var classlab03__main_1_1ADCTestObj =
[
    [ "__init__", "classlab03__main_1_1ADCTestObj.html#a626f7e411e5f29d78f782cfe4392dbe6", null ],
    [ "btnPressed", "classlab03__main_1_1ADCTestObj.html#a5d601f582906e54e0ae5d21ea1d7c3b0", null ],
    [ "handshake", "classlab03__main_1_1ADCTestObj.html#aa4a7cb93818fad8f76e653cd1cf61bfb", null ],
    [ "readADC", "classlab03__main_1_1ADCTestObj.html#af58026b50a6d47858de752ebd2f41be6", null ],
    [ "writeOutADC", "classlab03__main_1_1ADCTestObj.html#aec46566442717670bdf667637ed3db37", null ],
    [ "ADC", "classlab03__main_1_1ADCTestObj.html#aa8969408d3edb8bd1b58aa784090af2a", null ],
    [ "btnPressed_flag", "classlab03__main_1_1ADCTestObj.html#a64c1c6c854d892fa70e092dbd1e50560", null ],
    [ "buff", "classlab03__main_1_1ADCTestObj.html#a40e4e770035aa6259a085d24657c91cf", null ],
    [ "bus", "classlab03__main_1_1ADCTestObj.html#a6a42b3aad184531cd02eac6e319f2b1d", null ],
    [ "handshake_flag", "classlab03__main_1_1ADCTestObj.html#a3f1cd6f67eccea6120f23c6400daad7f", null ],
    [ "LED", "classlab03__main_1_1ADCTestObj.html#a60a805e669db4fdd1f6387c711aaeb23", null ],
    [ "timer", "classlab03__main_1_1ADCTestObj.html#a0faba145b81c512515eef746dbbba888", null ]
];