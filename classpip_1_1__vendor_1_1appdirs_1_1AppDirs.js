var classpip_1_1__vendor_1_1appdirs_1_1AppDirs =
[
    [ "__init__", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a49badd93c5b0ad1b540776a82bed7b78", null ],
    [ "site_config_dir", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a86d988d4209bb630427e87f11631136b", null ],
    [ "site_data_dir", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a228b7a1e3c0131fb42b6c270deb4f9df", null ],
    [ "user_cache_dir", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a5c37259df247dad522ee7681e49c8c5b", null ],
    [ "user_config_dir", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a986e24486a683d00b8fa749d947b4682", null ],
    [ "user_data_dir", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a7f447db02cb3c7928a274c747dc3e980", null ],
    [ "user_log_dir", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a393a7659050a995a3bc659670c7c269b", null ],
    [ "appauthor", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a7ce23aa82b9c9e1fd6fd6c034cb7e3c5", null ],
    [ "appname", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a1f68589425ae8dc5e12cf37dcd12a12c", null ],
    [ "multipath", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a650d66c20b9b7169efc78d6492097124", null ],
    [ "roaming", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a5c01b0bb6c8df8092454c523a28cd265", null ],
    [ "version", "classpip_1_1__vendor_1_1appdirs_1_1AppDirs.html#a53acc2f55f1d866332cc96cc17809c7a", null ]
];