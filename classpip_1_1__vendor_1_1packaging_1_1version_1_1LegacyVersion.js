var classpip_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion =
[
    [ "__init__", "classpip_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#ad36b8e92797aa978d1b1658e019aaf65", null ],
    [ "__repr__", "classpip_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a5f884695f13a9b2077f5b0029ac1c0b2", null ],
    [ "__str__", "classpip_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a6c2883cc191f134f8066910605dce7bc", null ],
    [ "base_version", "classpip_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a50772cdb6d45c175a38fda7ec6e5f8df", null ],
    [ "is_postrelease", "classpip_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a1fb443c341afc18ba217c234929ddcc3", null ],
    [ "is_prerelease", "classpip_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a0da8ea4118325e186966d2f98568baea", null ],
    [ "local", "classpip_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#aa9235448296342c68fdf10eb62df58d2", null ],
    [ "public", "classpip_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a9da147487f19fbce3ffee42c66349c8d", null ]
];