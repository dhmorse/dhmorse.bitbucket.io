var classpkg__resources_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion =
[
    [ "__init__", "classpkg__resources_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#ae5d3f571bca144067e99fc12c89f1814", null ],
    [ "__repr__", "classpkg__resources_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a0882c578a893a399d01ee90b3f218b5c", null ],
    [ "__str__", "classpkg__resources_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a12e3140dca0d1608e94532a8c4290bc3", null ],
    [ "base_version", "classpkg__resources_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#ada7b51696361b3339007d717a7eded4d", null ],
    [ "is_postrelease", "classpkg__resources_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#afb598a951757802d499b33a0d09fea29", null ],
    [ "is_prerelease", "classpkg__resources_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#af1c08627fa6f1cbfc1d09f2ccb48746a", null ],
    [ "local", "classpkg__resources_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a9ffd76cde683437bb134266d2480e23d", null ],
    [ "public", "classpkg__resources_1_1__vendor_1_1packaging_1_1version_1_1LegacyVersion.html#a3bc07656d9072933898f618d685113cf", null ]
];