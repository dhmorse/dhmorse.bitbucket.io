var classsetuptools_1_1msvc_1_1PlatformInfo =
[
    [ "__init__", "classsetuptools_1_1msvc_1_1PlatformInfo.html#aa98d6a4ffa306e85dba513ac18612492", null ],
    [ "cross_dir", "classsetuptools_1_1msvc_1_1PlatformInfo.html#a9cd7bd8ff66ddc3b79fe7448c52f1504", null ],
    [ "current_dir", "classsetuptools_1_1msvc_1_1PlatformInfo.html#a5d8f69638b5c98cc654174f4c944d1ab", null ],
    [ "current_is_x86", "classsetuptools_1_1msvc_1_1PlatformInfo.html#a09c58742017714dfae07c6604e3f837b", null ],
    [ "target_cpu", "classsetuptools_1_1msvc_1_1PlatformInfo.html#a2c7d1dc5add6f0b2494e161f364c4644", null ],
    [ "target_dir", "classsetuptools_1_1msvc_1_1PlatformInfo.html#a8ff098933fbab72ab767191b392e486b", null ],
    [ "target_is_x86", "classsetuptools_1_1msvc_1_1PlatformInfo.html#a33202c8f736ad183994d06b48b7afd6f", null ],
    [ "arch", "classsetuptools_1_1msvc_1_1PlatformInfo.html#a35417db9c47564d7101f92145515092c", null ],
    [ "target_cpu", "classsetuptools_1_1msvc_1_1PlatformInfo.html#a5bcf4cada8bfe500f75146ebcbfc1759", null ]
];