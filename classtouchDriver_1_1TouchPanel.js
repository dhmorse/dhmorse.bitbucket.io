var classtouchDriver_1_1TouchPanel =
[
    [ "__init__", "classtouchDriver_1_1TouchPanel.html#a93cfcaa9a311b44c626aa29c67b5cffb", null ],
    [ "filter", "classtouchDriver_1_1TouchPanel.html#a37bf8f2b9aad075c857fd8dedd573fd8", null ],
    [ "scan", "classtouchDriver_1_1TouchPanel.html#a59a434b7475f4ceff5b70030ee11980a", null ],
    [ "xScan", "classtouchDriver_1_1TouchPanel.html#ac59746ebe263070e20618d5816bf2374", null ],
    [ "yScan", "classtouchDriver_1_1TouchPanel.html#a95931bc5638c33e37e69c56c5b92b1f0", null ],
    [ "zScan", "classtouchDriver_1_1TouchPanel.html#a4bcf63aefbcfcb2da7c4c3a194d49cc5", null ],
    [ "delay", "classtouchDriver_1_1TouchPanel.html#a60958a87788fbaf607d9a176c2f286bb", null ],
    [ "fPin", "classtouchDriver_1_1TouchPanel.html#ae03de0722556e635b9a5c4b623fcb236", null ],
    [ "hiPin", "classtouchDriver_1_1TouchPanel.html#ad24a2ebc19fbba7fde6383989365a782", null ],
    [ "loPin", "classtouchDriver_1_1TouchPanel.html#aca74772d32d9cdb6a746f283c8e89af6", null ],
    [ "ox", "classtouchDriver_1_1TouchPanel.html#a9f0bdb6944c080e0651f0c71219e3004", null ],
    [ "oy", "classtouchDriver_1_1TouchPanel.html#a1c92f21d423892dbbc4f722812434d4b", null ],
    [ "p_len", "classtouchDriver_1_1TouchPanel.html#a8afef5679ab60d88a176d485bacd08c6", null ],
    [ "p_width", "classtouchDriver_1_1TouchPanel.html#a8f3c1d74408bb70e3568cdf42284257d", null ],
    [ "res", "classtouchDriver_1_1TouchPanel.html#a187ab11576ee661505db2638d11eaf5b", null ],
    [ "vPin", "classtouchDriver_1_1TouchPanel.html#a18cee350a7d637e895c76e239413e6e6", null ],
    [ "xm", "classtouchDriver_1_1TouchPanel.html#a24861e65069310dd1b26d838368e6632", null ],
    [ "xp", "classtouchDriver_1_1TouchPanel.html#a254c1691519dc7c9c61f9f1cea7d7cca", null ],
    [ "ym", "classtouchDriver_1_1TouchPanel.html#a6b700d037059ee315cc3806f12f78683", null ],
    [ "yp", "classtouchDriver_1_1TouchPanel.html#aaf6b9a4f5269c4219e55a760b6ee4a6e", null ]
];