var classtouchy_1_1touchy =
[
    [ "__init__", "classtouchy_1_1touchy.html#a916d676773503d15875d6ad3dbf2488b", null ],
    [ "allread", "classtouchy_1_1touchy.html#a916f39e4ed7cb5dffcb1cf5c3c18ba75", null ],
    [ "Xcoord", "classtouchy_1_1touchy.html#a2cc3a53e1ef1576035d3e51ac18f8014", null ],
    [ "Ycoord", "classtouchy_1_1touchy.html#a62184d38af6a57b5b42d056128efbb4b", null ],
    [ "Zbool", "classtouchy_1_1touchy.html#adb5ae72a4ad706c826732d7b0da75a68", null ],
    [ "Length", "classtouchy_1_1touchy.html#a4f6f531912be9dd9e3a19ae0f18778aa", null ],
    [ "Width", "classtouchy_1_1touchy.html#a0b723d3cfb27cc2b568fb2968792bfdf", null ],
    [ "X0", "classtouchy_1_1touchy.html#adfe7b5fa0d0489797ba697175b38cff5", null ],
    [ "Xm", "classtouchy_1_1touchy.html#ac159416a1eefa243df6cd751585f5670", null ],
    [ "Xmpin", "classtouchy_1_1touchy.html#aedd4b01b2f6e6986d02d6c9202b8030b", null ],
    [ "Xp", "classtouchy_1_1touchy.html#acbd15dc4085f0197f1134c9b7f4f6d40", null ],
    [ "Xppin", "classtouchy_1_1touchy.html#a6c894f0946ed95aad61c79a4a1ade25b", null ],
    [ "Y0", "classtouchy_1_1touchy.html#a892a11cd44abdabb29900ae590584360", null ],
    [ "Ym", "classtouchy_1_1touchy.html#a7e514574ed0a8e5ecf069ef84437f6c4", null ],
    [ "Ympin", "classtouchy_1_1touchy.html#ad93e43847386d54228817dbf85895939", null ],
    [ "Yp", "classtouchy_1_1touchy.html#a0c7ca3c082035fe83c64f0bbec6a98d9", null ],
    [ "Yppin", "classtouchy_1_1touchy.html#a6e727d7038fe7508dad0c42eb56d4f2e", null ]
];