var namespacepip_1_1baseparser =
[
    [ "PrettyHelpFormatter", "classpip_1_1baseparser_1_1PrettyHelpFormatter.html", "classpip_1_1baseparser_1_1PrettyHelpFormatter" ],
    [ "UpdatingDefaultsHelpFormatter", "classpip_1_1baseparser_1_1UpdatingDefaultsHelpFormatter.html", "classpip_1_1baseparser_1_1UpdatingDefaultsHelpFormatter" ],
    [ "CustomOptionParser", "classpip_1_1baseparser_1_1CustomOptionParser.html", "classpip_1_1baseparser_1_1CustomOptionParser" ],
    [ "ConfigOptionParser", "classpip_1_1baseparser_1_1ConfigOptionParser.html", "classpip_1_1baseparser_1_1ConfigOptionParser" ],
    [ "_environ_prefix_re", "namespacepip_1_1baseparser.html#a5ba8658646141bc6a0a347899c3cc605", null ]
];