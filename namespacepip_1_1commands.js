var namespacepip_1_1commands =
[
    [ "check", null, [
      [ "CheckCommand", "classpip_1_1commands_1_1check_1_1CheckCommand.html", "classpip_1_1commands_1_1check_1_1CheckCommand" ],
      [ "logger", "commands_2check_8py.html#a3d82a9875100841dc5fca4c15ecda1ce", null ]
    ] ],
    [ "completion", null, [
      [ "CompletionCommand", "classpip_1_1commands_1_1completion_1_1CompletionCommand.html", "classpip_1_1commands_1_1completion_1_1CompletionCommand" ],
      [ "BASE_COMPLETION", "completion_8py.html#a42d3677e84c4947181459d09f40e8ef0", null ],
      [ "COMPLETION_SCRIPTS", "completion_8py.html#a06feac68c13597ec3fa108ed320b24ab", null ]
    ] ],
    [ "download", null, [
      [ "DownloadCommand", "classpip_1_1commands_1_1download_1_1DownloadCommand.html", "classpip_1_1commands_1_1download_1_1DownloadCommand" ],
      [ "logger", "pip_2commands_2download_8py.html#a26173dbb34cbba034d66bcf01b8e0c51", null ]
    ] ],
    [ "freeze", null, [
      [ "FreezeCommand", "classpip_1_1commands_1_1freeze_1_1FreezeCommand.html", "classpip_1_1commands_1_1freeze_1_1FreezeCommand" ],
      [ "DEV_PKGS", "commands_2freeze_8py.html#ac8f3a476fab1cb298bac6d4a15bf5f00", null ]
    ] ],
    [ "hash", null, [
      [ "HashCommand", "classpip_1_1commands_1_1hash_1_1HashCommand.html", "classpip_1_1commands_1_1hash_1_1HashCommand" ],
      [ "_hash_of_file", "hash_8py.html#a7a17f6b3992d8ee895d9d68cb2aefa49", null ],
      [ "logger", "hash_8py.html#aa31610f4571538450a30f6c5c3d4f730", null ]
    ] ],
    [ "help", null, [
      [ "HelpCommand", "classpip_1_1commands_1_1help_1_1HelpCommand.html", "classpip_1_1commands_1_1help_1_1HelpCommand" ]
    ] ],
    [ "install", null, [
      [ "InstallCommand", "classpip_1_1commands_1_1install_1_1InstallCommand.html", "classpip_1_1commands_1_1install_1_1InstallCommand" ],
      [ "get_lib_location_guesses", "pip_2commands_2install_8py.html#a4292d10dd5c72252deb05cb2b750c2c8", null ],
      [ "logger", "pip_2commands_2install_8py.html#acfcff048c8cc28d587aa495beda3638b", null ],
      [ "wheel", "pip_2commands_2install_8py.html#a3444b754519ea80c66f8942952656251", null ]
    ] ],
    [ "list", null, [
      [ "ListCommand", "classpip_1_1commands_1_1list_1_1ListCommand.html", "classpip_1_1commands_1_1list_1_1ListCommand" ],
      [ "format_for_columns", "list_8py.html#a197585854739e5e7c6d02d3eb3e55eeb", null ],
      [ "format_for_json", "list_8py.html#a94998218d23a2605a7c1ac3c849260c7", null ],
      [ "tabulate", "list_8py.html#a828a23c945549303dc37dab4964f32c5", null ],
      [ "logger", "list_8py.html#ad49708b52e0eb8edc5402ed90ee69fe0", null ]
    ] ],
    [ "search", null, [
      [ "SearchCommand", "classpip_1_1commands_1_1search_1_1SearchCommand.html", "classpip_1_1commands_1_1search_1_1SearchCommand" ],
      [ "highest_version", "search_8py.html#a000646176a9f643c081a2e371e60e3a4", null ],
      [ "print_results", "search_8py.html#aa47cac5590d7a1d7b284d2441e398b2b", null ],
      [ "transform_hits", "search_8py.html#aa00a19f6ffdaf3df3e019203b7ba31b1", null ],
      [ "logger", "search_8py.html#ae76acd78f01d85ad3660494c3a9bda99", null ]
    ] ],
    [ "show", null, [
      [ "ShowCommand", "classpip_1_1commands_1_1show_1_1ShowCommand.html", "classpip_1_1commands_1_1show_1_1ShowCommand" ],
      [ "print_results", "show_8py.html#a7e252efe03311e675195d5bfbde2b6d5", null ],
      [ "search_packages_info", "show_8py.html#afd8a3b42fbe9b5e1f7c0b1f301ed08e0", null ],
      [ "logger", "show_8py.html#a991cd0a12d0cc7cc33b9c012859df7ca", null ]
    ] ],
    [ "uninstall", null, [
      [ "UninstallCommand", "classpip_1_1commands_1_1uninstall_1_1UninstallCommand.html", "classpip_1_1commands_1_1uninstall_1_1UninstallCommand" ]
    ] ],
    [ "wheel", null, [
      [ "WheelCommand", "classpip_1_1commands_1_1wheel_1_1WheelCommand.html", "classpip_1_1commands_1_1wheel_1_1WheelCommand" ],
      [ "logger", "pip_2commands_2wheel_8py.html#a5745ceba45a15fb39bfe31a4035d77d4", null ]
    ] ],
    [ "_sort_commands", "namespacepip_1_1commands.html#ae529cb1f97fdf51c52f6a906ade61aa1", null ],
    [ "get_similar_commands", "namespacepip_1_1commands.html#aa808d9413492b934afbb388391441a83", null ],
    [ "get_summaries", "namespacepip_1_1commands.html#ae39a8b6d0749edb37df29219a2225246", null ],
    [ "commands_dict", "namespacepip_1_1commands.html#abdfcfe4fe28aa0e9a57adf653463e19d", null ],
    [ "commands_order", "namespacepip_1_1commands.html#ae15a32dbe7607bb28041e940fd0e9107", null ]
];