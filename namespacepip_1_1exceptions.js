var namespacepip_1_1exceptions =
[
    [ "PipError", "classpip_1_1exceptions_1_1PipError.html", null ],
    [ "InstallationError", "classpip_1_1exceptions_1_1InstallationError.html", null ],
    [ "UninstallationError", "classpip_1_1exceptions_1_1UninstallationError.html", null ],
    [ "DistributionNotFound", "classpip_1_1exceptions_1_1DistributionNotFound.html", null ],
    [ "RequirementsFileParseError", "classpip_1_1exceptions_1_1RequirementsFileParseError.html", null ],
    [ "BestVersionAlreadyInstalled", "classpip_1_1exceptions_1_1BestVersionAlreadyInstalled.html", null ],
    [ "BadCommand", "classpip_1_1exceptions_1_1BadCommand.html", null ],
    [ "CommandError", "classpip_1_1exceptions_1_1CommandError.html", null ],
    [ "PreviousBuildDirError", "classpip_1_1exceptions_1_1PreviousBuildDirError.html", null ],
    [ "InvalidWheelFilename", "classpip_1_1exceptions_1_1InvalidWheelFilename.html", null ],
    [ "UnsupportedWheel", "classpip_1_1exceptions_1_1UnsupportedWheel.html", null ],
    [ "HashErrors", "classpip_1_1exceptions_1_1HashErrors.html", "classpip_1_1exceptions_1_1HashErrors" ],
    [ "HashError", "classpip_1_1exceptions_1_1HashError.html", "classpip_1_1exceptions_1_1HashError" ],
    [ "VcsHashUnsupported", "classpip_1_1exceptions_1_1VcsHashUnsupported.html", null ],
    [ "DirectoryUrlHashUnsupported", "classpip_1_1exceptions_1_1DirectoryUrlHashUnsupported.html", null ],
    [ "HashMissing", "classpip_1_1exceptions_1_1HashMissing.html", "classpip_1_1exceptions_1_1HashMissing" ],
    [ "HashUnpinned", "classpip_1_1exceptions_1_1HashUnpinned.html", null ],
    [ "HashMismatch", "classpip_1_1exceptions_1_1HashMismatch.html", "classpip_1_1exceptions_1_1HashMismatch" ],
    [ "UnsupportedPythonVersion", "classpip_1_1exceptions_1_1UnsupportedPythonVersion.html", null ]
];