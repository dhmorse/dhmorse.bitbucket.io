var namespacepip_1_1utils_1_1deprecation =
[
    [ "PipDeprecationWarning", "classpip_1_1utils_1_1deprecation_1_1PipDeprecationWarning.html", null ],
    [ "Pending", "classpip_1_1utils_1_1deprecation_1_1Pending.html", null ],
    [ "RemovedInPip10Warning", "classpip_1_1utils_1_1deprecation_1_1RemovedInPip10Warning.html", null ],
    [ "RemovedInPip11Warning", "classpip_1_1utils_1_1deprecation_1_1RemovedInPip11Warning.html", null ],
    [ "Python26DeprecationWarning", "classpip_1_1utils_1_1deprecation_1_1Python26DeprecationWarning.html", null ],
    [ "_showwarning", "namespacepip_1_1utils_1_1deprecation.html#a5f66045c32ae5b87ee95225011b6d967", null ],
    [ "install_warning_logger", "namespacepip_1_1utils_1_1deprecation.html#aebecdca01b2f785cc3d1ff1102ee0ec8", null ],
    [ "_warnings_showwarning", "namespacepip_1_1utils_1_1deprecation.html#af7a530c3d88863241adb9190e3a49d5c", null ]
];