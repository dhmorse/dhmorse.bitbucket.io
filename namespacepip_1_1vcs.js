var namespacepip_1_1vcs =
[
    [ "bazaar", null, [
      [ "Bazaar", "classpip_1_1vcs_1_1bazaar_1_1Bazaar.html", "classpip_1_1vcs_1_1bazaar_1_1Bazaar" ],
      [ "logger", "bazaar_8py.html#a6140262b36a6ad0e64cc7e3edcd2d223", null ]
    ] ],
    [ "git", null, [
      [ "Git", "classpip_1_1vcs_1_1git_1_1Git.html", "classpip_1_1vcs_1_1git_1_1Git" ],
      [ "logger", "git_8py.html#af1c467e2d37a9c757bfb21b7a8636072", null ],
      [ "urlsplit", "git_8py.html#a09a2c1351210ec9865ac16885f5a5b50", null ],
      [ "urlunsplit", "git_8py.html#a890c46e2e96bebe9df6c1394d88a3b4a", null ]
    ] ],
    [ "mercurial", null, [
      [ "Mercurial", "classpip_1_1vcs_1_1mercurial_1_1Mercurial.html", "classpip_1_1vcs_1_1mercurial_1_1Mercurial" ],
      [ "logger", "mercurial_8py.html#a6bc3f006a528f5e6032aba3f7ac65c0b", null ]
    ] ],
    [ "subversion", null, [
      [ "Subversion", "classpip_1_1vcs_1_1subversion_1_1Subversion.html", "classpip_1_1vcs_1_1subversion_1_1Subversion" ],
      [ "get_rev_options", "subversion_8py.html#aba0433ef84de9effb107ad73c2c03318", null ],
      [ "_svn_info_xml_rev_re", "subversion_8py.html#a321d35b6c360a15b751a997fb3061c24", null ],
      [ "_svn_info_xml_url_re", "subversion_8py.html#a17a7ebd678a820945db3ffce2ff55ca5", null ],
      [ "_svn_rev_re", "subversion_8py.html#a8ac3f5681b89eb0596d44c51dd7ad766", null ],
      [ "_svn_revision_re", "subversion_8py.html#a90d5bba0d4646f9524e3f7e076b8b409", null ],
      [ "_svn_url_re", "subversion_8py.html#aa13a38ad76a9c76c2bfd368dffb1835d", null ],
      [ "_svn_xml_url_re", "subversion_8py.html#a213fb44bad1e098b22bf45007786b419", null ],
      [ "logger", "subversion_8py.html#a91690c24b639a63f8cabf5c88742a27c", null ]
    ] ],
    [ "VcsSupport", "classpip_1_1vcs_1_1VcsSupport.html", "classpip_1_1vcs_1_1VcsSupport" ],
    [ "VersionControl", "classpip_1_1vcs_1_1VersionControl.html", "classpip_1_1vcs_1_1VersionControl" ],
    [ "get_src_requirement", "namespacepip_1_1vcs.html#a941e9bbbc4b5d1217df18a914e5d83af", null ],
    [ "__all__", "namespacepip_1_1vcs.html#aff4e7f87e6eaf0b3f9f5f6e0a404dad1", null ],
    [ "logger", "namespacepip_1_1vcs.html#a6fd5bbd1830c95e9fd08c4b4818e2f78", null ],
    [ "vcs", "namespacepip_1_1vcs.html#a9664772c6e3f60852c46644e09235d36", null ]
];