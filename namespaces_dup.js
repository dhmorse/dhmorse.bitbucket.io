var namespaces_dup =
[
    [ "lab01", "namespacelab01.html", "namespacelab01" ],
    [ "lab02", "namespacelab02.html", "namespacelab02" ],
    [ "lab03_main", "namespacelab03__main.html", "namespacelab03__main" ],
    [ "lab03_ui", "namespacelab03__ui.html", [
      [ "main", "namespacelab03__ui.html#a0276397099968a6cad4004a34ac76568", null ],
      [ "plotData", "namespacelab03__ui.html#a685f66688e7f719faf251c4b4d0439f2", null ],
      [ "serWrite", "namespacelab03__ui.html#ab2240537d497721d54dd655708a9298f", null ],
      [ "startUI", "namespacelab03__ui.html#a3afd9e6abb6e736265f7c0dce05deae6", null ]
    ] ],
    [ "lab04_main", "namespacelab04__main.html", [
      [ "getInternalTemp", "namespacelab04__main.html#a78a0f08fc82e6bbe0ff60925ce669f6d", null ],
      [ "main", "namespacelab04__main.html#a00138593117ed4598def6d77af1c6cd3", null ]
    ] ],
    [ "lab04_mcp9808", "namespacelab04__mcp9808.html", "namespacelab04__mcp9808" ],
    [ "lab04_ui", "namespacelab04__ui.html", [
      [ "main", "namespacelab04__ui.html#a605fdd0c3ce5fd9e391fb1bc1decc0c6", null ],
      [ "parseCSV", "namespacelab04__ui.html#a5cd79438717adfdc6f27a59b779ebc68", null ],
      [ "plotData", "namespacelab04__ui.html#a0ef80e854ae4802a77a5c36fbc0053a7", null ]
    ] ],
    [ "lab07", "namespacelab07.html", "namespacelab07" ],
    [ "mainpage", "namespacemainpage.html", null ],
    [ "termProject", "namespacetermProject.html", null ]
];