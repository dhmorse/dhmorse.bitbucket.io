var namespacesetuptools =
[
    [ "archive_util", "namespacesetuptools_1_1archive__util.html", "namespacesetuptools_1_1archive__util" ],
    [ "command", null, [
      [ "alias", null, [
        [ "alias", "classsetuptools_1_1command_1_1alias_1_1alias.html", "classsetuptools_1_1command_1_1alias_1_1alias" ],
        [ "format_alias", "alias_8py.html#a3723fa7e97193ce0a08db94393418d25", null ],
        [ "shquote", "alias_8py.html#ab7dcb5208d78b13962adc7fe0c257e10", null ]
      ] ],
      [ "bdist_egg", "namespacesetuptools_1_1command_1_1bdist__egg.html", "namespacesetuptools_1_1command_1_1bdist__egg" ],
      [ "bdist_rpm", null, [
        [ "bdist_rpm", "classsetuptools_1_1command_1_1bdist__rpm_1_1bdist__rpm.html", "classsetuptools_1_1command_1_1bdist__rpm_1_1bdist__rpm" ]
      ] ],
      [ "bdist_wininst", null, [
        [ "bdist_wininst", "classsetuptools_1_1command_1_1bdist__wininst_1_1bdist__wininst.html", "classsetuptools_1_1command_1_1bdist__wininst_1_1bdist__wininst" ]
      ] ],
      [ "build_ext", null, [
        [ "build_ext", "classsetuptools_1_1command_1_1build__ext_1_1build__ext.html", "classsetuptools_1_1command_1_1build__ext_1_1build__ext" ],
        [ "_customize_compiler_for_shlib", "build__ext_8py.html#addb7f4c420e7875ca96014144db2f2fb", null ],
        [ "_get_config_var_837", "build__ext_8py.html#a54ce493201e328931cbe6f9daef85fb4", null ],
        [ "get_abi3_suffix", "build__ext_8py.html#ae6906ec7a016e0980a3d09aaacbc0489", null ],
        [ "link_shared_object", "build__ext_8py.html#af6495dcefb19a333c4fba87af82b45c6", null ],
        [ "_build_ext", "build__ext_8py.html#a608cc05f8cd4907b6a14eb5971f7876d", null ],
        [ "have_rtld", "build__ext_8py.html#a72aec6a687c5be3341dc37d8b017c4c9", null ],
        [ "if_dl", "build__ext_8py.html#a827f3b3ab6de6e389a77d491f0e14dda", null ],
        [ "libtype", "build__ext_8py.html#ac22854f97d7214647dc62d6748af00fb", null ],
        [ "use_stubs", "build__ext_8py.html#a2a27c3423f10a8f057c2f558bb7e7dab", null ]
      ] ],
      [ "build_py", null, [
        [ "Mixin2to3", "classsetuptools_1_1command_1_1build__py_1_1Mixin2to3.html", "classsetuptools_1_1command_1_1build__py_1_1Mixin2to3" ],
        [ "build_py", "classsetuptools_1_1command_1_1build__py_1_1build__py.html", "classsetuptools_1_1command_1_1build__py_1_1build__py" ],
        [ "_unique_everseen", "build__py_8py.html#a1dd8fb92407a8484b7cf09ec5fb4c53e", null ],
        [ "assert_relative", "build__py_8py.html#a1c5a9e809e618a17fb86cec7fcbdc25b", null ]
      ] ],
      [ "develop", null, [
        [ "develop", "classsetuptools_1_1command_1_1develop_1_1develop.html", "classsetuptools_1_1command_1_1develop_1_1develop" ],
        [ "VersionlessRequirement", "classsetuptools_1_1command_1_1develop_1_1VersionlessRequirement.html", "classsetuptools_1_1command_1_1develop_1_1VersionlessRequirement" ]
      ] ],
      [ "easy_install", "namespacesetuptools_1_1command_1_1easy__install.html", "namespacesetuptools_1_1command_1_1easy__install" ],
      [ "egg_info", "namespacesetuptools_1_1command_1_1egg__info.html", "namespacesetuptools_1_1command_1_1egg__info" ],
      [ "install", null, [
        [ "install", "classsetuptools_1_1command_1_1install_1_1install.html", "classsetuptools_1_1command_1_1install_1_1install" ],
        [ "_install", "setuptools_2command_2install_8py.html#a1e1b936dc01098bb7e9fde038bc871b9", null ]
      ] ],
      [ "install_egg_info", null, [
        [ "install_egg_info", "classsetuptools_1_1command_1_1install__egg__info_1_1install__egg__info.html", "classsetuptools_1_1command_1_1install__egg__info_1_1install__egg__info" ]
      ] ],
      [ "install_lib", null, [
        [ "install_lib", "classsetuptools_1_1command_1_1install__lib_1_1install__lib.html", "classsetuptools_1_1command_1_1install__lib_1_1install__lib" ]
      ] ],
      [ "install_scripts", null, [
        [ "install_scripts", "classsetuptools_1_1command_1_1install__scripts_1_1install__scripts.html", "classsetuptools_1_1command_1_1install__scripts_1_1install__scripts" ]
      ] ],
      [ "py36compat", null, [
        [ "sdist_add_defaults", "classsetuptools_1_1command_1_1py36compat_1_1sdist__add__defaults.html", "classsetuptools_1_1command_1_1py36compat_1_1sdist__add__defaults" ]
      ] ],
      [ "register", null, [
        [ "register", "classsetuptools_1_1command_1_1register_1_1register.html", "classsetuptools_1_1command_1_1register_1_1register" ]
      ] ],
      [ "rotate", null, [
        [ "rotate", "classsetuptools_1_1command_1_1rotate_1_1rotate.html", "classsetuptools_1_1command_1_1rotate_1_1rotate" ]
      ] ],
      [ "saveopts", null, [
        [ "saveopts", "classsetuptools_1_1command_1_1saveopts_1_1saveopts.html", "classsetuptools_1_1command_1_1saveopts_1_1saveopts" ]
      ] ],
      [ "sdist", null, [
        [ "sdist", "classsetuptools_1_1command_1_1sdist_1_1sdist.html", "classsetuptools_1_1command_1_1sdist_1_1sdist" ],
        [ "walk_revctrl", "sdist_8py.html#a2c73b1c3e885ffe491953225a8b5a130", null ],
        [ "_default_revctrl", "sdist_8py.html#aec95fcc27c22554c1a73083f3c9b47bd", null ]
      ] ],
      [ "setopt", null, [
        [ "option_base", "classsetuptools_1_1command_1_1setopt_1_1option__base.html", "classsetuptools_1_1command_1_1setopt_1_1option__base" ],
        [ "setopt", "classsetuptools_1_1command_1_1setopt_1_1setopt.html", "classsetuptools_1_1command_1_1setopt_1_1setopt" ],
        [ "config_file", "setopt_8py.html#ad752b37cad3757f6b9457e752a74f87a", null ],
        [ "edit_config", "setopt_8py.html#aea3f5581fe84f5821452ac30e78dd66a", null ],
        [ "__all__", "setopt_8py.html#a3cf4762e8e8fb05dbb87ac63f822f9da", null ]
      ] ],
      [ "test", null, [
        [ "ScanningLoader", "classsetuptools_1_1command_1_1test_1_1ScanningLoader.html", "classsetuptools_1_1command_1_1test_1_1ScanningLoader" ],
        [ "NonDataProperty", "classsetuptools_1_1command_1_1test_1_1NonDataProperty.html", "classsetuptools_1_1command_1_1test_1_1NonDataProperty" ],
        [ "test", "classsetuptools_1_1command_1_1test_1_1test.html", "classsetuptools_1_1command_1_1test_1_1test" ]
      ] ],
      [ "upload", null, [
        [ "upload", "classsetuptools_1_1command_1_1upload_1_1upload.html", "classsetuptools_1_1command_1_1upload_1_1upload" ]
      ] ],
      [ "upload_docs", null, [
        [ "upload_docs", "classsetuptools_1_1command_1_1upload__docs_1_1upload__docs.html", "classsetuptools_1_1command_1_1upload__docs_1_1upload__docs" ],
        [ "_encode", "upload__docs_8py.html#a5c39e46f3df02b340f77998e02f91bdd", null ]
      ] ],
      [ "__all__", "setuptools_2command_2____init_____8py.html#a9849f1fc79c2a58181a879d859f1a1d3", null ]
    ] ],
    [ "depends", null, [
      [ "Require", "classsetuptools_1_1depends_1_1Require.html", "classsetuptools_1_1depends_1_1Require" ],
      [ "_iter_code", "depends_8py.html#a4f3ed04532365c857ff2282f9dea0b25", null ],
      [ "_update_globals", "depends_8py.html#aaf80c343ae8c08a76171eee4f94c2708", null ],
      [ "extract_constant", "depends_8py.html#a49bf4636dee480cc02cef0d9e81ca05e", null ],
      [ "find_module", "depends_8py.html#a4a94ecfdec67b2f432eed142f7cafaf5", null ],
      [ "get_module_constant", "depends_8py.html#a5a13c841cb5ec0ec368eb437b3e7c615", null ],
      [ "__all__", "depends_8py.html#a12fce1ae40d81e3952ed133393279752", null ]
    ] ],
    [ "dist", null, [
      [ "Distribution", "classsetuptools_1_1dist_1_1Distribution.html", "classsetuptools_1_1dist_1_1Distribution" ],
      [ "Feature", "classsetuptools_1_1dist_1_1Feature.html", "classsetuptools_1_1dist_1_1Feature" ],
      [ "_get_unpatched", "dist_8py.html#a117a32e8e02cc88c46b7cbd11b66d73b", null ],
      [ "assert_bool", "dist_8py.html#a6248cc8e03decc97c768ba201e3a3244", null ],
      [ "assert_string_list", "dist_8py.html#adef1021a792040663598a45748f8cf95", null ],
      [ "check_entry_points", "dist_8py.html#a392bacff0b7eefe57d55ac19706234c1", null ],
      [ "check_extras", "dist_8py.html#a67dcebb09dd6485db2f8850dfe3045f3", null ],
      [ "check_importable", "dist_8py.html#a34c7bc2fb87e3610638e5e71cbbebb9c", null ],
      [ "check_nsp", "dist_8py.html#a328a9faed0ff4a67c79bf755bb8aa9c5", null ],
      [ "check_package_data", "dist_8py.html#a1c396158f859904995bff0393651fe59", null ],
      [ "check_packages", "dist_8py.html#ae1e38f1e592688eba2adffd9498dba7b", null ],
      [ "check_requirements", "dist_8py.html#a29a45dac0119a4bfaf9412a216a6fd41", null ],
      [ "check_specifier", "dist_8py.html#ad16e6b95535ea80403747b5ef21cd66d", null ],
      [ "check_test_suite", "dist_8py.html#a6a197d513c80b32120484d6c7dd29066", null ],
      [ "write_pkg_file", "dist_8py.html#a37d5624a94cb0491805ecd6dddba85bc", null ],
      [ "write_pkg_info", "dist_8py.html#a7e266b8822f0a0702b118544cc667552", null ],
      [ "__all__", "dist_8py.html#a330aa0a5773145da29fcc29112d8d5d6", null ],
      [ "_Distribution", "dist_8py.html#aa0cb85b707e6bdf7d64465f53723f238", null ],
      [ "sequence", "dist_8py.html#a990181c5330d133fd66d47c179a9395b", null ]
    ] ],
    [ "extension", null, [
      [ "Extension", "classsetuptools_1_1extension_1_1Extension.html", "classsetuptools_1_1extension_1_1Extension" ],
      [ "Library", "classsetuptools_1_1extension_1_1Library.html", null ],
      [ "_have_cython", "extension_8py.html#a03c57f086187c61a88e5cf91c584e749", null ],
      [ "_Extension", "extension_8py.html#a67f61d1eb8077b0421f9604eb409fc3b", null ],
      [ "have_pyrex", "extension_8py.html#ade5fed76cd45f46d1f2124e5034923d3", null ]
    ] ],
    [ "extern", null, [
      [ "names", "setuptools_2extern_2____init_____8py.html#ad7afd4e9bfb782dd48e6313701aa54c9", null ]
    ] ],
    [ "glob", "namespacesetuptools_1_1glob.html", [
      [ "_iglob", "namespacesetuptools_1_1glob.html#a93af55dca0c5beeb96c626f4300f01fc", null ],
      [ "_isrecursive", "namespacesetuptools_1_1glob.html#a181565b7c30afb53b2aa63945d0b6e3d", null ],
      [ "_rlistdir", "namespacesetuptools_1_1glob.html#ae9f785c3019c40278e20c3d6c231f122", null ],
      [ "escape", "namespacesetuptools_1_1glob.html#a73ffea4dfe02e8a0294dad20dbb55a0a", null ],
      [ "glob", "namespacesetuptools_1_1glob.html#aaa3481017d8575f370da9768113e795f", null ],
      [ "glob0", "namespacesetuptools_1_1glob.html#a75dda51129cf0b110589765487cb12dd", null ],
      [ "glob1", "namespacesetuptools_1_1glob.html#a44ead1132e5a984ad1edd86a4b643141", null ],
      [ "glob2", "namespacesetuptools_1_1glob.html#a314c02b7732b13b3a0c129a7282b55ef", null ],
      [ "has_magic", "namespacesetuptools_1_1glob.html#aab80b9560e1118922c32b88a7b758288", null ],
      [ "iglob", "namespacesetuptools_1_1glob.html#a785709f6dd54bc19304bb581cc6e7707", null ],
      [ "__all__", "namespacesetuptools_1_1glob.html#ad922fbfc51551d658ee1c168c3a97dd8", null ],
      [ "magic_check", "namespacesetuptools_1_1glob.html#a8b4aa8dfe849c42d139011a8050da833", null ],
      [ "magic_check_bytes", "namespacesetuptools_1_1glob.html#a6004b75354404430fa928ea3dd63792c", null ]
    ] ],
    [ "launch", "namespacesetuptools_1_1launch.html", [
      [ "run", "namespacesetuptools_1_1launch.html#af5305aff3ea020e40c0696ae7a071650", null ]
    ] ],
    [ "lib2to3_ex", "namespacesetuptools_1_1lib2to3__ex.html", "namespacesetuptools_1_1lib2to3__ex" ],
    [ "monkey", "namespacesetuptools_1_1monkey.html", [
      [ "_patch_distribution_metadata_write_pkg_file", "namespacesetuptools_1_1monkey.html#ada4187bd22dbda7338c4abc03e5c4187", null ],
      [ "_patch_distribution_metadata_write_pkg_info", "namespacesetuptools_1_1monkey.html#a5515aa918d92282aa0d050282bf95958", null ],
      [ "get_unpatched", "namespacesetuptools_1_1monkey.html#aca0a412fd577e44c2a0d0847019103db", null ],
      [ "get_unpatched_class", "namespacesetuptools_1_1monkey.html#ad46b685f3cc7245cde194b4b7831b38a", null ],
      [ "get_unpatched_function", "namespacesetuptools_1_1monkey.html#a394451d3a77b5f2ff32bc108d7b677d4", null ],
      [ "patch_all", "namespacesetuptools_1_1monkey.html#a593eccd6bead483dc9479ed13d1c4f16", null ],
      [ "patch_for_msvc_specialized_compiler", "namespacesetuptools_1_1monkey.html#a496bc1769e481213444b1d53185b1b89", null ],
      [ "patch_func", "namespacesetuptools_1_1monkey.html#ac6c1bb45a1d90ebcaf099ada0682616b", null ],
      [ "__all__", "namespacesetuptools_1_1monkey.html#aff2bcbc62f469ca7ac4ff8e52b80f33b", null ]
    ] ],
    [ "msvc", "namespacesetuptools_1_1msvc.html", "namespacesetuptools_1_1msvc" ],
    [ "namespaces", null, [
      [ "Installer", "classsetuptools_1_1namespaces_1_1Installer.html", "classsetuptools_1_1namespaces_1_1Installer" ],
      [ "DevelopInstaller", "classsetuptools_1_1namespaces_1_1DevelopInstaller.html", null ],
      [ "flatten", "namespaces_8py.html#ac526d0a5bfb5f02e06baaacd8ea73a8a", null ]
    ] ],
    [ "package_index", "namespacesetuptools_1_1package__index.html", "namespacesetuptools_1_1package__index" ],
    [ "py26compat", "namespacesetuptools_1_1py26compat.html", [
      [ "import_module", "namespacesetuptools_1_1py26compat.html#a85c44731576f3aa731c1e21bd8bc0738", null ],
      [ "strip_fragment", "namespacesetuptools_1_1py26compat.html#a545769ded247930681bcec9c026efdd8", null ],
      [ "strip_fragment", "namespacesetuptools_1_1py26compat.html#a7004a6d15579920923c72e93005fa073", null ]
    ] ],
    [ "py27compat", "namespacesetuptools_1_1py27compat.html", [
      [ "get_all_headers", "namespacesetuptools_1_1py27compat.html#ac25365180a68bc60bacc66eb0c05f4b6", null ]
    ] ],
    [ "py31compat", null, [
      [ "TemporaryDirectory", "classsetuptools_1_1py31compat_1_1TemporaryDirectory.html", "classsetuptools_1_1py31compat_1_1TemporaryDirectory" ],
      [ "get_path", "py31compat_8py.html#a7542646cbc287c1878ab7d9320435f15", null ],
      [ "unittest_main", "py31compat_8py.html#ae401ebf7ac1927cca11236acb396889c", null ],
      [ "__all__", "py31compat_8py.html#a1104d06f25209ce1a2f56550ccc24cec", null ],
      [ "_PY31", "py31compat_8py.html#a00ac352ccd435ba5b5d678e9408df64e", null ],
      [ "unittest_main", "py31compat_8py.html#acb943bc1c3ff699123d78a6b23581c5b", null ]
    ] ],
    [ "sandbox", null, [
      [ "UnpickleableException", "classsetuptools_1_1sandbox_1_1UnpickleableException.html", null ],
      [ "ExceptionSaver", "classsetuptools_1_1sandbox_1_1ExceptionSaver.html", "classsetuptools_1_1sandbox_1_1ExceptionSaver" ],
      [ "AbstractSandbox", "classsetuptools_1_1sandbox_1_1AbstractSandbox.html", "classsetuptools_1_1sandbox_1_1AbstractSandbox" ],
      [ "DirectorySandbox", "classsetuptools_1_1sandbox_1_1DirectorySandbox.html", "classsetuptools_1_1sandbox_1_1DirectorySandbox" ],
      [ "SandboxViolation", "classsetuptools_1_1sandbox_1_1SandboxViolation.html", "classsetuptools_1_1sandbox_1_1SandboxViolation" ],
      [ "_clear_modules", "sandbox_8py.html#ad662e655b0da6125a13d1d5bad99d44f", null ],
      [ "_execfile", "sandbox_8py.html#a04db34a27158ecd99d947180df80ae78", null ],
      [ "_needs_hiding", "sandbox_8py.html#abb2f68a1e768f720045421d10ae2c91a", null ],
      [ "hide_setuptools", "sandbox_8py.html#a2866b4e88f03f6075fed0f7a530261c3", null ],
      [ "override_temp", "sandbox_8py.html#a2c4ba056356a14d170e0b4b60fd02580", null ],
      [ "pushd", "sandbox_8py.html#a7971928fb0fbf584da85d366379232d3", null ],
      [ "run_setup", "sandbox_8py.html#ac27deefbd9b5163ee6861eb07b93bc23", null ],
      [ "save_argv", "sandbox_8py.html#a2a79966d30bf09df3b132b92a596738c", null ],
      [ "save_modules", "sandbox_8py.html#a03b1ef3f49621d5d24d9847920d2a71a", null ],
      [ "save_path", "sandbox_8py.html#a057c8a6e653446a00924ea4809b63dc6", null ],
      [ "save_pkg_resources_state", "sandbox_8py.html#a7f34d1ea59a8595bed11a0c2f0e84aef", null ],
      [ "setup_context", "sandbox_8py.html#a52b79978a4d33340bc744648198b5a87", null ],
      [ "__all__", "sandbox_8py.html#a866546c157bec8341d08592db0cd0277", null ],
      [ "_EXCEPTIONS", "sandbox_8py.html#aa68adb36abe8accb61bfff7f33e49462", null ],
      [ "_file", "sandbox_8py.html#a606d2df05a9166bff220807da3b98823", null ],
      [ "_open", "sandbox_8py.html#a616fa17a666518a02267233ecdc9df33", null ],
      [ "_os", "sandbox_8py.html#a99f75779e14f881f51f4c38d4a586167", null ],
      [ "WRITE_FLAGS", "sandbox_8py.html#ae2d3c0e479bb79bd56a7f884d5a5826b", null ]
    ] ],
    [ "site-patch", null, [
      [ "__boot", "site-patch_8py.html#ad8f8f3980efade3d54cf34aa50b1485e", null ]
    ] ],
    [ "ssl_support", null, [
      [ "CertificateError", "classsetuptools_1_1ssl__support_1_1CertificateError.html", null ],
      [ "VerifyingHTTPSHandler", "classsetuptools_1_1ssl__support_1_1VerifyingHTTPSHandler.html", "classsetuptools_1_1ssl__support_1_1VerifyingHTTPSHandler" ],
      [ "VerifyingHTTPSConn", "classsetuptools_1_1ssl__support_1_1VerifyingHTTPSConn.html", "classsetuptools_1_1ssl__support_1_1VerifyingHTTPSConn" ],
      [ "_dnsname_match", "ssl__support_8py.html#a43ecb57b8ac191993ca9fbcd2e2b5805", null ],
      [ "find_ca_bundle", "ssl__support_8py.html#a52d0214adb152f4c38aebc413e404278", null ],
      [ "get_win_certfile", "ssl__support_8py.html#a057bf5d0a699f8fba4f40ac7c339185d", null ],
      [ "match_hostname", "ssl__support_8py.html#af946fca9376289edb47b2e1babe9f9ee", null ],
      [ "opener_for", "ssl__support_8py.html#ace7164bc765a0d578ae9c82eeb76b13a", null ],
      [ "__all__", "ssl__support_8py.html#ad9834cffaa5a18aa4ee561b6fc6fd9c6", null ],
      [ "_wincerts", "ssl__support_8py.html#ab469239b315e811781c21bb0cc23af78", null ],
      [ "cert_paths", "ssl__support_8py.html#a3b596fad36adc8396fb3eebc33b00cda", null ],
      [ "CertificateError", "ssl__support_8py.html#ac1a38bae98615d399c0145a6efb165e5", null ],
      [ "HTTPSConnection", "ssl__support_8py.html#a2080acb14b60edc35201cff1915da4f8", null ],
      [ "HTTPSHandler", "ssl__support_8py.html#af7187d08f1ac5df0ac0382808f2a47d5", null ],
      [ "is_available", "ssl__support_8py.html#a64bb9b0c9a63f24544b55c1d91312464", null ],
      [ "match_hostname", "ssl__support_8py.html#a8a8e98ff0935d8cda04e2f19fd235bc2", null ],
      [ "ssl", "ssl__support_8py.html#a55a27a1705e0f1696f44078a212ed844", null ]
    ] ],
    [ "unicode_utils", null, [
      [ "decompose", "unicode__utils_8py.html#a0d73a9ef29c9b2362952427baf268cf9", null ],
      [ "filesys_decode", "unicode__utils_8py.html#a1b815569db9e78559bb4d6bd478e98df", null ],
      [ "try_encode", "unicode__utils_8py.html#aa8cee81a221e36526de668605979ef31", null ]
    ] ],
    [ "version", null, [
      [ "__version__", "setuptools_2version_8py.html#a51b31dc5559b6d9cfa276da08a440b6c", null ]
    ] ],
    [ "windows_support", null, [
      [ "hide_file", "windows__support_8py.html#abd52492d8f2879fc041e17badfbe02f7", null ],
      [ "windows_only", "windows__support_8py.html#a2cb3d8ebd26448a87d18595fe56b92c8", null ]
    ] ],
    [ "PackageFinder", "classsetuptools_1_1PackageFinder.html", "classsetuptools_1_1PackageFinder" ],
    [ "PEP420PackageFinder", "classsetuptools_1_1PEP420PackageFinder.html", null ],
    [ "Command", "classsetuptools_1_1Command.html", "classsetuptools_1_1Command" ],
    [ "_find_all_simple", "namespacesetuptools.html#aa3a9df45c7fcb70c8082c9462dd37ad6", null ],
    [ "findall", "namespacesetuptools.html#a9d3e6b5f82f1d22fb1d7ad0daf6018f6", null ],
    [ "__all__", "namespacesetuptools.html#a4225bf1f017388e9cad84b5c0618a3b0", null ],
    [ "__version__", "namespacesetuptools.html#a032cd4f48c18d83d8bb090942751a9cc", null ],
    [ "_Command", "namespacesetuptools.html#adb63f7c592fd2900f321f933980a8b86", null ],
    [ "bootstrap_install_from", "namespacesetuptools.html#a7d51d85e64a02e0808685340753eec38", null ],
    [ "find_packages", "namespacesetuptools.html#af3f752d8ed43dd0bf1640292cf7f2540", null ],
    [ "lib2to3_fixer_packages", "namespacesetuptools.html#a2a9aa3f4107914fedf47fbf0b6c97620", null ],
    [ "run_2to3_on_doctests", "namespacesetuptools.html#a579fb1acef3ca217a470e0a0a1659503", null ],
    [ "setup", "namespacesetuptools.html#a3347a1832befe1bf0e593b4c8bf7c9f8", null ]
];