var namespacesetuptools_1_1msvc =
[
    [ "winreg", "classsetuptools_1_1msvc_1_1winreg.html", null ],
    [ "PlatformInfo", "classsetuptools_1_1msvc_1_1PlatformInfo.html", "classsetuptools_1_1msvc_1_1PlatformInfo" ],
    [ "RegistryInfo", "classsetuptools_1_1msvc_1_1RegistryInfo.html", "classsetuptools_1_1msvc_1_1RegistryInfo" ],
    [ "SystemInfo", "classsetuptools_1_1msvc_1_1SystemInfo.html", "classsetuptools_1_1msvc_1_1SystemInfo" ],
    [ "EnvironmentInfo", "classsetuptools_1_1msvc_1_1EnvironmentInfo.html", "classsetuptools_1_1msvc_1_1EnvironmentInfo" ],
    [ "_augment_exception", "namespacesetuptools_1_1msvc.html#a2e2729a21a08d13ae5a901107cc8edc8", null ],
    [ "msvc14_gen_lib_options", "namespacesetuptools_1_1msvc.html#ae8eaedaea82a9ef4bf775d23d60a712e", null ],
    [ "msvc14_get_vc_env", "namespacesetuptools_1_1msvc.html#abf4312c4a50e38b0965b836584cf402c", null ],
    [ "msvc9_find_vcvarsall", "namespacesetuptools_1_1msvc.html#a4ee903451bc75bf16dbdd14461ed230f", null ],
    [ "msvc9_query_vcvarsall", "namespacesetuptools_1_1msvc.html#a0bd796bce555440ffd1f4760f97d7601", null ],
    [ "safe_env", "namespacesetuptools_1_1msvc.html#ac539bcfcdbc99c49e78edc78e6b4c9d5", null ]
];