/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "405 Documentation", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Course Assignments", "index.html#sec_assignments", null ],
    [ "LABS", "pg_labs.html", [
      [ "Labs", "pg_labs.html#sec_labs", null ],
      [ "Lab01: Reintroduction", "pg_labs.html#sec_lab01", null ],
      [ "Lab02: Hardware Familiarization", "pg_labs.html#sec_lab02", null ],
      [ "Lab03: Communication via Serial", "pg_labs.html#sec_lab03", null ],
      [ "Lab04: Temperature Readings via I2C", "pg_labs.html#sec_lab04", null ]
    ] ],
    [ "Lab01: Reintroduction", "pg_lab01.html", [
      [ "Finite State Machine Diagram", "pg_lab01.html#sec_lab01_FSM", null ],
      [ "Vending Machine FSM Code", "pg_lab01.html#sec_lab01_Code", null ]
    ] ],
    [ "Lab02: Hardware Familiarization", "pg_lab02.html", [
      [ "The Game", "pg_lab02.html#sec_lab02_Game", null ]
    ] ],
    [ "Lab03: Communication via Serial", "pg_lab03.html", null ],
    [ "Lab04: Temperature Readings via I2C", "pg_lab04.html", [
      [ "Temperature Plot", "pg_lab04.html#sec_lab04_tempsPlot", null ]
    ] ],
    [ "TERM PROJECT", "termProject_8py.html", [
      [ "Term Project Labs:", "termProject_8py.html#sec_projParts", null ]
    ] ],
    [ "Lab05: Pivoting Platform Kinematic Model", "pg_lab05.html", [
      [ "Hand Calcs", "pg_lab05.html#sec_lab05_Work", null ]
    ] ],
    [ "Lab06: Simulation or Reality?", "pg_lab06.html", [
      [ "Sections:", "pg_lab06.html#sec_lab06_Sections", null ],
      [ "Scenarios", "pg_lab06.html#sec_lab06_Plots", null ],
      [ "MATLAB Results", "pg_lab06.html#sec_lab06_Matlab", null ]
    ] ],
    [ "Lab07: Feeling Touchy", "pg_lab07.html", [
      [ "Hardware Setup", "pg_lab07.html#sec_l7hardware", null ],
      [ "Results:", "pg_lab07.html#sec_l7results", null ]
    ] ],
    [ "Lab08: Motor and Encoder Drivers", "pg_lab08.html", [
      [ "Encoder", "pg_lab08.html#sec_lab08_Encoder", null ],
      [ "Motor Driver and Base", "pg_lab08.html#sec_lab08_Motordriver", null ]
    ] ],
    [ "Lab09: All Together Now", "pg_lab09.html", [
      [ "Contents:", "pg_lab09.html#sec_lab09_Contents", null ],
      [ "How it Works", "pg_lab09.html#sec_lab09_HowItWorks", null ],
      [ "Results", "pg_lab09.html#sec_lab09_Results", null ],
      [ "Tuned Gains", "pg_lab09.html#sec_lab09_Gainstuned", null ],
      [ "The Attempts", "pg_lab09.html#sec_lab09_Attempts", null ],
      [ "Discussion", "pg_lab09.html#sec_lab09_Discussion", null ],
      [ "Gain Calculations", "pg_lab09.html#sec_lab09_Calculations", null ],
      [ "Links", "pg_lab09.html#sec_lab09_Links", [
        [ "Videos", "pg_lab09.html#ssec_lab09_videos", null ],
        [ "Source Code", "pg_lab09.html#ssec_lab09_source", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BalancingFunctionalityAttempt_8py.html",
"mainThatBalancedtheTable_8py.html#ab7a2a66da8fc6a8371331367e4ad3106"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';