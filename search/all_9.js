var searchData=
[
  ['lab01_2epy_48',['lab01.py',['../lab01_8py.html',1,'']]],
  ['lab02_2epy_49',['lab02.py',['../lab02_8py.html',1,'']]],
  ['lab07_2epy_50',['lab07.py',['../lab07_8py.html',1,'']]],
  ['lab09_5fmain_2epy_51',['lab09_main.py',['../lab09__main_8py.html',1,'']]],
  ['length_52',['Length',['../classtouchy_1_1touchy.html#a4f6f531912be9dd9e3a19ae0f18778aa',1,'touchy::touchy']]],
  ['lengthpast_53',['lengthpast',['../BalancingFunctionalityAttempt_8py.html#a527f8eec559efbda801a504c1f37680c',1,'BalancingFunctionalityAttempt']]],
  ['lspeed_54',['lspeed',['../BalancingFunctionalityAttempt_8py.html#a64f1213c080dccffb9a4eebd430457ff',1,'BalancingFunctionalityAttempt']]],
  ['lab01_3a_20reintroduction_55',['Lab01: Reintroduction',['../pg_lab01.html',1,'']]],
  ['lab02_3a_20hardware_20familiarization_56',['Lab02: Hardware Familiarization',['../pg_lab02.html',1,'']]],
  ['lab03_3a_20communication_20via_20serial_57',['Lab03: Communication via Serial',['../pg_lab03.html',1,'']]],
  ['lab04_3a_20temperature_20readings_20via_20i2c_58',['Lab04: Temperature Readings via I2C',['../pg_lab04.html',1,'']]],
  ['lab05_3a_20pivoting_20platform_20kinematic_20model_59',['Lab05: Pivoting Platform Kinematic Model',['../pg_lab05.html',1,'']]],
  ['lab06_3a_20simulation_20or_20reality_3f_60',['Lab06: Simulation or Reality?',['../pg_lab06.html',1,'']]],
  ['lab07_3a_20feeling_20touchy_61',['Lab07: Feeling Touchy',['../pg_lab07.html',1,'']]],
  ['lab08_3a_20motor_20and_20encoder_20drivers_62',['Lab08: Motor and Encoder Drivers',['../pg_lab08.html',1,'']]],
  ['lab09_3a_20all_20together_20now_63',['Lab09: All Together Now',['../pg_lab09.html',1,'']]],
  ['labs_64',['LABS',['../pg_labs.html',1,'']]]
];
